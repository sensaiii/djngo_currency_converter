import json
from django.conf import settings
import redis
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
import requests
from .services import get_latest_rates
import serialized_redis

# Connect to our Redis instance
redis_instance = serialized_redis.JSONSerializedRedis(host=settings.REDIS_HOST,
                                                      port=settings.REDIS_PORT, db=0)


@api_view(['GET'])
def currencies(request, *args, **kwargs):
    items = []
    for key in redis_instance.keys("*"):
        # items[key.decode("utf-8")]= redis_instance.hget(key)
        print(key)
        items.append(key)

        data = redis_instance.mget(["INR", "AUD", "CAD", "JPY"])
        print(data)

    # if len(items) == 0:
        currencies = ["INR", "AUD", "CAD", "JPY"]
        for currency in currencies:
            latest_rates = get_latest_rates(currency)
            key = currency
            value = latest_rates['rates']
            print(value)
            val = json.dumps(value)

            redis_instance.set(key, val)

        res = json.dumps(currencies)
        print(res)
        return HttpResponse(res, 'application/json')

    response = {
        "keys": items

    }

    return Response(response, status=200)


@api_view(['GET'])
def convertion_rates(request, *args, **kwargs):
    data = redis_instance.get("INR")
    print(data)

    if request.method == "GET":

        value = redis_instance.get(kwargs['key'])
        print(value)
        if value:
            response = {
                'currency': kwargs['key'],
                'rates': json.loads(value)
            }
            return Response(response, status=200)

        else:
            response = {
                'key': kwargs['key'],
                'value': None,
                'msg': 'Not found'
            }
            return HttpResponse(response, status=404)

    """ except:

        resp={
           'key' : kwargs['key'],
           'value':None,
           'msg':' key Not found'
        }

        return Response(resp,status=  404) """
