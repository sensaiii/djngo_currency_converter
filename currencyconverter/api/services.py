import json
import requests
def get_latest_rates(currency_type):
    url='https://api.exchangeratesapi.io/latest'
    params={'base':currency_type}
    r=requests.get(url,params=params)
    rates=r.json()
    latest_rates={'currency':rates['base'],'rates':rates['rates']}
    return latest_rates