from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import currencies, convertion_rates

urlpatterns = {
    path('currencies', currencies, name="currencies"),
    path('currencies/<slug:key>', convertion_rates, name="currency")
}
urlpatterns = format_suffix_patterns(urlpatterns)